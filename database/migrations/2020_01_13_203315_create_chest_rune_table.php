<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChestRuneTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chest_rune', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('chest_id')->unsigned()->index();
            $table->bigInteger('rune_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('chest_rune', function ($table) {
            $table->foreign('chest_id')->references('id')->on('chests');
            $table->foreign('rune_id')->references('id')->on('runes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chest_rune');
    }
}
