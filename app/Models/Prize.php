<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    protected $fillable = [
        'name',
        'image',
        'price',
        'code',
    ];

    public function chests()
    {
        return $this->belongsToMany('App\Models\Chest', 'chest_prize', 'chest_id', 'prize_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'prize_user', 'prize_id', 'user_id');
    }
}
