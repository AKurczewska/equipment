<?php

namespace App\Http\Resources;

use App\Models\Rune;
use Illuminate\Http\Resources\Json\ResourceCollection;

class RuneCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Rune $rune) {
            return (new RuneResource($rune));
        });

        return parent::toArray($request);
    }
}
