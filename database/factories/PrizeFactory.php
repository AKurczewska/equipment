<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Prize;
use Faker\Generator as Faker;

$factory->define(Prize::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'image' => 'https://placeimg.com/100/100/any?' . rand(1, 100),
        'price' => $faker->numberBetween(1000, 5000),
        'code' => $faker->isbn10(),
    ];
});
