<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Chest extends Model
{
    protected $fillable = [
        'name',
        'image',
        'price',
    ];

    public function runes() {
        return $this->belongsToMany('App\Models\Rune', 'chest_rune', 'chest_id', 'rune_id');
    }

    public function prizes() {
        return $this->belongsToMany('App\Models\Prize', 'chest_prize', 'chest_id', 'prize_id');
    }

    public function users() {
        return $this->belongsToMany('App\Models\User', 'prize_user', 'prize_id', 'user_id');
    }
}
