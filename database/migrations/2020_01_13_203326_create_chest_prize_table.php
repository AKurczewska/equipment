<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChestPrizeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chest_prize', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('chest_id')->unsigned()->index();
            $table->bigInteger('prize_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('chest_prize', function ($table) {
            $table->foreign('chest_id')->references('id')->on('chests');
            $table->foreign('prize_id')->references('id')->on('prizes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chest_prize');
    }
}
