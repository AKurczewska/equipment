<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Chest;
use Faker\Generator as Faker;

$factory->define(Chest::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'image' => 'https://placeimg.com/100/100/any?' . rand(1, 100),
        'price' => $faker->numberBetween(500, 5000),
    ];
});
