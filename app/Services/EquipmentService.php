<?php

namespace App\Http\Services;

use App\Http\Requests\ChestBuyRequest;
use App\Http\Requests\PrizeBuyRequest;
use App\Http\Requests\RuneBuyRequest;
use App\Models\Chest;
use App\Models\Prize;
use App\Models\Rune;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class EquipmentService
{
    public function equipmentsList()
    {
        $equipment = User::with('chests')->with('prizes')->with('runes')->where('users.id', Auth::id())->first();

        return $equipment;
    }

    public function buyChest(ChestBuyRequest $request)
    {
        $data = $request->validated();
        $user = Auth::user();

        if ($user->chests->contains($data['id']))
            return abort(409, 'You already have this chest!');

        $chest = Chest::find($data['id']);
        $this->subtractSoulGems($chest->price);

        $user->chests()->attach($data['id']);
    }

    public function buyPrize(PrizeBuyRequest $request)
    {
        $data = $request->validated();
        $user = Auth::user();

        if (Auth::user()->prizes->contains($data['id']))
            return abort(409, 'You already have this prize!');

        $prize = Prize::find($data['id']);
        if (isset($data['code']) && $prize->code != $data['code']) {
            return abort(400, 'Wrong code!');
        } else {
            $this->subtractSoulGems($prize->price);
        }

        $user->prizes()->attach([$data['id'] => ['status' => 0]]);
    }

    public function buyRune(RuneBuyRequest $request)
    {
        $data = $request->validated();
        $user = Auth::user();

        if ($user->runes->contains($data['id']))
            return abort(409, 'You already have this rune!');

        $rune = Rune::find($data['id']);
        $this->subtractSoulGems($rune->price);

        $user->runes()->attach($data['id']);
    }

    private function subtractSoulGems(int $price)
    {
        $user = Auth::user();
        if ($user->soul_gems < $price)
            return abort(409, 'You don\'t have enough soul gems');
        $user->update(['soul_gems' => $user->soul_gems - $price]);
    }
}
