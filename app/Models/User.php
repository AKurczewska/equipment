<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    protected $fillable = [
        'name',
        'email',
        'password',
        'soul_gems',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function runes()
    {
        return $this->belongsToMany('App\Models\Rune', 'rune_user', 'rune_id', 'user_id');
    }

    public function prizes()
    {
        return $this->belongsToMany('App\Models\Prize', 'prize_user', 'prize_id', 'user_id')
            ->withPivot('status');
    }

    public function chests()
    {
        return $this->belongsToMany('App\Models\Chest', 'chest_user', 'chest_id', 'user_id');
    }
}
