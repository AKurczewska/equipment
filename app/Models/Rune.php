<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rune extends Model
{
    protected $fillable = [
        'name',
        'image',
        'price',
        'bonus',
    ];

    public function chests()
    {
        return $this->belongsToMany('App\Models\Chest', 'chest_rune', 'chest_id', 'rune_id');
    }

    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'rune_user', 'rune_id', 'user_id');
    }
}
