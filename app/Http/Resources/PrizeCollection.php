<?php

namespace App\Http\Resources;

use App\Models\Prize;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PrizeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $this->collection->transform(function (Prize $prize) {
            return (new PrizeResource($prize));
        });

        return parent::toArray($request);
    }
}
