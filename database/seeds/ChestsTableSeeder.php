<?php

use App\Models\Chest;
use Illuminate\Database\Seeder;

class ChestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Chest::class, 10)->create();

        $chests = Chest::all();
        foreach($chests as $chest) {
            $chest->runes()->sync([rand(1, 10), rand(1, 10)]);
            $chest->prizes()->sync([rand(1, 10), rand(1, 10)]);
        }
    }
}
